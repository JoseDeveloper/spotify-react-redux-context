import React, { useState, useEffect } from 'react';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';
import { userSearches } from 'services/database/searches';
import Searches from './customHooks/searchs';
import DataTable from './customHooks/dataTable';
import DataContext from './context/dataContext';
import { getMyPublicIp } from './utils/getMyIp';

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Searches', isActive: true },
];

const UserSearchArtist = () => {
  const [textSearch, setTextSearch] = useState('');
  const [userIp, setUserIp] = useState(0);
  const [artistAlbums, setArtistAlbums] = useState([]);
  const [userSearchs, setUserSearchs] = useState([]);
  
  useEffect(() => {
    getMyIp();
    getUserSearchs();

  }, []);

  const getUserSearchs = async () =>  {
    const response = await userSearches.getUserSearchs(); 
    setUserSearchs(response);
  }

  const getMyIp = async () => {
    const ipv4 = await getMyPublicIp()
    setUserIp(ipv4);

  }

  const onSubmit = async () => {

    const response = await userSearches.searchAlbumsArtist(textSearch, userIp); 
    setArtistAlbums(response);
    getUserSearchs();
  }

  return (
    <DataContext.Provider value={{artistAlbums, userSearchs}}>
      <>
        <PageContainer heading={<IntlMessages id="pages.searches" />} breadcrumbs={breadcrumbs}>
          <GridContainer>
            <Grid item xs={12}>
              <Box>
                <IntlMessages id="pages.searches" />
              </Box>
            </Grid>
            <TableContainer component={Paper}>

              <form>
                <Box ml={5} width={230}>
                  <TextField
                    label={<IntlMessages id="pages.textSearch" />}
                    fullWidth
                    onChange={event => setTextSearch(event.target.value)}
                    defaultValue={textSearch}
                    margin="normal"
                    variant="outlined"
                  />
                </Box>

                <Searches />

                <Box ml={5} display="flex" alignItems="center" justifyContent="space-between" mb={5}>
                  <Button onClick={onSubmit} variant="contained" color="primary">
                    <IntlMessages id="pages.submit" />
                  </Button>
                </Box>
              </form>

              <DataTable />

            </TableContainer>
          </GridContainer>
        </PageContainer>
      </>
    </DataContext.Provider>
  );
};

export default UserSearchArtist;
