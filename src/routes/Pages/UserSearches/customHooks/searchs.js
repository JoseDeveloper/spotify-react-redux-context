import React, { useContext } from 'react';
import Box from '@material-ui/core/Box';
import Chip from '@material-ui/core/Chip';
import DataContext from '../context/dataContext';

const Searches = () => {
  const { userSearchs } = useContext(DataContext);

  return (
    <>
        <Box ml={5} m={8}>
            <div className="row">
                {userSearchs && userSearchs.map((item)=> {
                    return <Chip label={`${item.search}`} className = "col-md-1 p-3" variant="outlined" />
                })}
                  
            </div>
        </Box>
    </>
  );
};

export default Searches;
