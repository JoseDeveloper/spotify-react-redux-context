import React, { useContext } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import { Avatar } from '@material-ui/core';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import DataContext from '../context/dataContext';

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});

const DataTable = () => {
  const classes = useStyles();
  const { artistAlbums } = useContext(DataContext);

  return (
        <TableContainer component={Paper}> 
            <Table className={classes.table} aria-label="Table">
                <TableHead>
                    <TableRow>
                        <TableCell>Image </TableCell>
                        <TableCell>Album</TableCell>
                        <TableCell>Tracks</TableCell>
                        <TableCell>Release date</TableCell>
                        <TableCell>Popularity</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody> 
                    {artistAlbums && artistAlbums.map((row, n) => (
                        <TableRow key={n}>
                            <TableCell component="th" scope="row">
                                <Avatar alt="Remy Sharp" src={`${row.cover}`} />
                            </TableCell>
                             <TableCell component="th" scope="row">
                                {row.albumName} 
                            </TableCell>
              
                            <TableCell component="th" scope="row">
                                {row.totalTracks}
                            </TableCell>
                            <TableCell component="th" scope="row">
                                {row.releaseDate}
                            </TableCell>
                            <TableCell component="th" scope="row">
                                {row.popularity}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
  
};

export default DataTable;