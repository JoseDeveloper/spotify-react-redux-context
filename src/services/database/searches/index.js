import axios from '../../config';

export const userSearches = {
  searchAlbumsArtist: (textSearch, userIp) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.get(`user-searches/searchArtistAlbums/${textSearch}/${userIp}`, {})
      .then(({ data }) => {
        if (data) {
        
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },

  getUserSearchs: () => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.get('user-searches/')
      .then(({ data }) => {
        if (data) {
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },
};

