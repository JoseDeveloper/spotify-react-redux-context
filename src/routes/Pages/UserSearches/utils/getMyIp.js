const publicIp = require("react-public-ip");

export const getMyPublicIp = async () => {
    return await publicIp.v4() || "";
}